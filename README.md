### Iron man, the best MCU character ###

Demo website I knocked up after being excited and watching the avengers. 
 
* The copy text didn't get added appropriately as I got sidetracked after starting and focused almost exclusively on trying to make the site as smooth as possible without having to compromise animation or image quality.
* Can be viewed [here via Netlify](https://unruffled-meitner-a7e870.netlify.com/)