// So that the website doesnt do anything until it has a chance to load

var menuOpen = true;
var suitUpPlayed = false;



var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy",
    load_delay: 300 //adjust according to use case
});



// ----------------
var buttonEl    = document.querySelector('#menu .button');
var lineEl      = document.querySelector('#menu .line');
var parts       = $("#menu .items").children();
var menuItem    = $('#menu .menuItem')[0]

function animateButton(scale, duration, elasticity) {
  anime.remove(buttonEl);
  anime({
    targets: buttonEl,
    scale: scale,
    duration: duration,
    elasticity: elasticity
  });
}

function animateLine(scale, duration, elasticity, opacity) {
    anime.remove(lineEl);
    anime({
        targets: lineEl,
        scaleY: scale,
        opacity: opacity,
        duration: duration,
        elasticity: elasticity
    });
}

function functionAnimatePart(item, duration, opacity, leftMov, delay, animIn) {

    if (animIn === true) { $(item).css({ display: 'block' }) };

    const anim = animIn;

    anime.remove(item);
    anime({
        targets: item,
        opacity: opacity,
        duration: duration,
        translateX: leftMov,
        delay: delay,
        complete: function() { 
            if (anim === false) {
                $(item).css({ display: 'none' }); 
                item.removeEventListener('click', function() { gotoSection() } )
            } else {
                const data = $(item).data();
                const section = data.section;
                console.log(section)
                item.addEventListener('click', function() { gotoSection(section) }, false )
            }
        }
    });
}


function gotoSection(section) {
    $("html, body").animate({ scrollTop: $(`#${section}`).offset().top - 200 }, 300);
}


function animateItemHover(item, scale, duration, elasticity, leftMov) {
    anime.remove(item);
    anime({
      targets: item,
      scale: scale,
      duration: duration,
      elasticity: elasticity,
      translateX: leftMov,
    });
  }


function animateItems(animIn, set_duration, set_delay) {

    var duration    = set_duration || 800;
    var opacity     = (animIn === true) ? 1 : 0;
    var leftMov     = (animIn === true) ? 0 : 20;
    //var display     = (animIn === true) ? 'static' : 'none';

    for (var i = 0; i < parts.length; i ++) {
        
        const item = parts[i]

        var delay = (set_delay || 20) * i;
        functionAnimatePart(item, duration, opacity, leftMov, delay, animIn);

        function enterItem() { animateItemHover(item, 1.2, 800, 400, leftMov) };
        function leaveItem() { animateItemHover(item, 1.0, 600, 300, leftMov) };

        if (animIn === true) {
            item.addEventListener('mouseenter', enterItem, false);
            item.addEventListener('mouseleave', leaveItem, false);
        } else {
            item.removeEventListener('mouseenter', enterItem, false);
            item.removeEventListener('mouseleave', leaveItem, false);
        }


    }
}

function animateMenuItem(animeIn) {

    var rotate  =   (animeIn === true) ? 90 : 0;
    var scale   =   (animeIn === true) ? 1.5 : 1;

    anime.remove(menuItem);
    anime({
      targets: menuItem,
      rotate: rotate,
      scale: scale
    });
}

function toggleMenu() { 

    if (menuOpen === true) {
        animateLine(1, 1000, 0, 1);
        animateMenuItem(true);
        animateItems(true);
        menuOpen = false;
    }
    else if (menuOpen === false) {
        animateLine(1, 1000, 300, 0);
        animateMenuItem(false)
        animateItems(false)
        menuOpen = true;
    }
 }



 //$("html, body").animate({ scrollTop: $('#title1').offset().top }, 1000);


//animateLine(1, 0, 300, 0)
//animateItems(false, 0, 0)

function enterButton() { animateButton(1.2, 800, 400) };
function leaveButton() { animateButton(1.0, 600, 300) };

buttonEl.addEventListener('mouseenter', enterButton, false);
buttonEl.addEventListener('mouseleave', leaveButton, false);
buttonEl.addEventListener('click', toggleMenu, false);






// ----------------






// INTRO --------
anime({
    targets: '.content',
    opacity: 1
})


// Intro text ---
anime({
    delay: 200,
    targets: '#introText',
    //translateY: [250, 0], // -> '250px'
    opacity: [0, 1],
    //rotate: [-540, 0], // -> '540deg'
    scale: [2, 1],
    easing: 'easeInOutQuad'
})



$(document).ready(function() {
    suitUp.restart();
    suitUp.pause();
})



$(window).scroll(function() {
    var hT = $('#suit').offset().top + ($(window).height() / 1.5) ,
        hH = $('#suit').outerHeight(),
        wH = $(window).height(),
        wS = $(this).scrollTop();
    console.log(wH)

        offset = wH / 2;
    if (wS > (hT+hH-wH )){

        if (suitUpPlayed === true) return;
        //console.log('H1 on the view!');
        suitUpPlayed = true;
        suitUp.play();
    } 
 });







// SUIT UP ANIMTAION ---------------------------
var suitUp = anime.timeline({
    easing: 'easeOutQuart',
    //delay: 1,
    //duration: 5
})


// SUIT / TONY COmbo --------


suitUp.add({
    targets: '#suit .tony_arm_left_lower',
    opacity: [1, 0],
    translateX: -15,
    //delay: 400,
    //duration: 400
}, 0);

suitUp.add({
    targets: '#suit .tony_arm_left_upper',
    opacity: [1, 0],
    //delay: 400,
    translateX: -15,
    //duration: 400
}, 1);

suitUp.add({
    targets: '#suit .tony_leg_left_lower',
    opacity: [1, 0],
    translateX: -15,
    //delay: 400,
    //duration: 400
}, 1);

suitUp.add({
    targets: '#suit .tony_leg_left_upper',
    opacity: [1, 0],
    delay: 400,
    translateX: -15,
    //duration: 400
}, 1)

suitUp.add({
    targets: '#suit .tony_arm_right_lower',
    opacity: [1, 0],
    translateX: 15,
    delay: 400,
    //duration: 400
}, 0)

suitUp.add({
    targets: '#suit .tony_arm_right_upper',
    opacity: [1, 0],
    delay: 400,
    translateX: 15,
    //duration: 400
}, 0)

suitUp.add({
    targets: '#suit .tony_leg_right_lower',
    opacity: [1, 0],
    translateX: 15,
    //delay: 400,
    //duration: 400
}, 0)

suitUp.add({
    targets: '#suit .tony_leg_right_upper',
    opacity: [1, 0],
    //delay: 400,
    translateX: 15,
    //duration: 400
}, 0)

suitUp.add({
    targets: '#suit .tony_head',
    opacity: [1, 0],
    //delay: 500,
    //translateY: 15,
    //duration: 400
}, 0)




// SUIT ------

// LEFT -----
suitUp.add({
    targets: '#suit .suit_arm_left_lower',
    opacity: [0, 1],
    translateX: [-200, 0],
    translateY: [50, 0],
    //rotate: [-90, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0);


suitUp.add({
    //delay: 50,
    targets: '#suit .suit_arm_left_upper',
    opacity: [0, 1],
    translateX: [-300, 0],
    translateY: [-100, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)

suitUp.add({
    targets: '#suit .suit_leg_left_lower',
    opacity: [0, 1],
    translateX: [-200, 0],
    translateY: [300, 0],
    //rotate: [-90, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)


suitUp.add({
    //delay: 50,
    targets: '#suit .suit_leg_left_upper',
    opacity: [0, 1],
    translateX: [-300, 0],
    translateY: [-100, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)

suitUp.add({

    targets: '#suit .suit_left_shoulder',
    opacity: [0, 1],
    translateX: [-10, 0],
    translateY: [-200, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)

suitUp.add({

    targets: '#suit .suit_right_shoulder',
    opacity: [0, 1],
    translateX: [100, 0],
    translateY: [-200, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)


// CENTRAL ------

suitUp.add({

    targets: '#suit .suit_head',
    opacity: [0, 1],
    //translateX: [50, 0],
    //scale: [4, 1],
    translateX: [10, 0],
    translateY: [-140, 0],
    //rotate: [-10, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)


suitUp.add({

    targets: '#suit .suit_chest_lower',
    opacity: [0, 1],
    translateX: [50, 0],
    translateY: [100, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)

suitUp.add({

    targets: '#suit .suit_chest_upper',
    opacity: [0, 1],
    //translateX: [-40, 0],
    translateY: [-80, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)


// RIGHT ----

suitUp.add({
    targets: '#suit .suit_arm_right_upper',
    opacity: [0, 1],
    translateX: [400, 0],
    translateY: [-80, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)

suitUp.add({
    targets: '#suit .suit_arm_right_lower',
    opacity: [0, 1],
    translateX: [400, 0],
    translateY: [80, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)




suitUp.add({
    targets: '#suit .suit_leg_right_upper',
    opacity: [0, 1],
    translateX: [200, 0],
    translateY: [-10, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)

suitUp.add({
    targets: '#suit .suit_leg_right_lower',
    opacity: [0, 1],
    translateX: [300, 0],
    translateY: [300, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
}, 0)



// Images on intro
// Tony ----



// Ironman ----




// ---------



/*


// SUIT / TONY COmbo --------


anime({
    targets: '#suit .tony_arm_left_lower',
    opacity: [1, 0],
    translateX: -15,
    delay: 400,
    //duration: 400
})

anime({
    targets: '#suit .tony_arm_left_upper',
    opacity: [1, 0],
    delay: 400,
    translateX: -15,
    //duration: 400
})

anime({
    targets: '#suit .tony_leg_left_lower',
    opacity: [1, 0],
    translateX: -15,
    delay: 400,
    //duration: 400
})

anime({
    targets: '#suit .tony_leg_left_upper',
    opacity: [1, 0],
    delay: 400,
    translateX: -15,
    //duration: 400
})

anime({
    targets: '#suit .tony_arm_right_lower',
    opacity: [1, 0],
    translateX: 15,
    delay: 400,
    //duration: 400
})

anime({
    targets: '#suit .tony_arm_right_upper',
    opacity: [1, 0],
    delay: 400,
    translateX: 15,
    //duration: 400
})

anime({
    targets: '#suit .tony_leg_right_lower',
    opacity: [1, 0],
    translateX: 15,
    delay: 400,
    //duration: 400
})

anime({
    targets: '#suit .tony_leg_right_upper',
    opacity: [1, 0],
    delay: 400,
    translateX: 15,
    //duration: 400
})

anime({
    targets: '#suit .tony_head',
    opacity: [1, 0],
    delay: 500,
    //translateY: 15,
    //duration: 400
})




// SUIT ------

// LEFT -----
anime({
    targets: '#suit .suit_arm_left_lower',
    opacity: [0, 1],
    translateX: [-200, 0],
    translateY: [50, 0],
    //rotate: [-90, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})


anime({
    //delay: 50,
    targets: '#suit .suit_arm_left_upper',
    opacity: [0, 1],
    translateX: [-300, 0],
    translateY: [-100, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})

anime({
    targets: '#suit .suit_leg_left_lower',
    opacity: [0, 1],
    translateX: [-200, 0],
    translateY: [300, 0],
    //rotate: [-90, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})


anime({
    //delay: 50,
    targets: '#suit .suit_leg_left_upper',
    opacity: [0, 1],
    translateX: [-300, 0],
    translateY: [-100, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})

anime({

    targets: '#suit .suit_left_shoulder',
    opacity: [0, 1],
    translateX: [-10, 0],
    translateY: [-200, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})

anime({

    targets: '#suit .suit_right_shoulder',
    opacity: [0, 1],
    translateX: [100, 0],
    translateY: [-200, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})


// CENTRAL ------

anime({

    targets: '#suit .suit_head',
    opacity: [0, 1],
    //translateX: [50, 0],
    //scale: [4, 1],
    translateX: [10, 0],
    translateY: [-140, 0],
    //rotate: [-10, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})


anime({

    targets: '#suit .suit_chest_lower',
    opacity: [0, 1],
    translateX: [50, 0],
    translateY: [100, 0],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})

anime({

    targets: '#suit .suit_chest_upper',
    opacity: [0, 1],
    //translateX: [-40, 0],
    translateY: [-80, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})


// RIGHT ----

anime({
    targets: '#suit .suit_arm_right_upper',
    opacity: [0, 1],
    translateX: [400, 0],
    translateY: [-80, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})

anime({
    targets: '#suit .suit_arm_right_lower',
    opacity: [0, 1],
    translateX: [400, 0],
    translateY: [80, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})




anime({
    targets: '#suit .suit_leg_right_upper',
    opacity: [0, 1],
    translateX: [200, 0],
    translateY: [-10, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})

anime({
    targets: '#suit .suit_leg_right_lower',
    opacity: [0, 1],
    translateX: [300, 0],
    translateY: [300, 0],
    sacle: [2, 1],
    //rotate: [-180, 0],

    //duration: 200,
    easing: 'easeOutQuart'
})



// Images on intro
// Tony ----



// Ironman ----




// ---------


*/